import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { Column, Entity, OneToMany } from 'typeorm';
import { AuthOtp } from '../../auth/entities/auth-otp.entity';
import { ERoles } from '../../auth/enums';
import BaseEntity from '../../__shared__/interfaces/base.entity';

@Entity()
export class User extends BaseEntity {
  @Column({ unique: true, nullable: false })
  @ApiProperty()
  email: string;

  @Exclude()
  @Column({ nullable: false })
  password?: string;

  @Exclude()
  @Column({ nullable: true })
  refreshToken: string;

  @Column()
  @ApiProperty()
  name: string;

  @Column({ unique: true, nullable: true })
  @ApiProperty()
  phone: string;

  @Column({ default: false, nullable: true })
  @ApiProperty()
  verified: boolean;

  @Column({ default: true, nullable: false })
  @ApiProperty()
  active: boolean;

  @Column({ default: ERoles, nullable: false })
  @ApiProperty()
  role: ERoles;

  @OneToMany(() => AuthOtp, (otp: AuthOtp) => otp.user)
  public otp: AuthOtp[];
}
