import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { paginate } from 'nestjs-typeorm-paginate';
import { Repository } from 'typeorm';
import { EmailService } from '../communication/email.service';
import { ConfirmationEmailTemplate } from '../__shared__/email-templates/confirmation-email';
import { IPage, IPagination } from '../__shared__/interfaces/page.interface';
import { generatePassword } from '../__shared__/utils/password-generator';
import { PasswordEncryption } from '../__shared__/utils/PasswordEncryption';
import { CreateUserAccountDto } from './dto/create-user-account.dto';
import { UpdateUserAccountDto } from './dto/update-user-account.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private readonly emailService: EmailService,
    private readonly configService: ConfigService,
    private readonly passwordEncryption: PasswordEncryption,
  ) {}
  async create(createUserAccountDto: CreateUserAccountDto): Promise<any> {
    const randPassword = generatePassword();
    const password = await this.passwordEncryption.hashPassword(randPassword);
    const results = await this.userRepository.save({
      ...createUserAccountDto,
      password,
    });
    const confirmationMail = {
      to: results.email,
      subject: 'Trust seal account confirmation',
      from: this.configService.get('sendgrid').fromEmail,
      text: `Account creation confirmation`,
      html: ConfirmationEmailTemplate(results.name.split(' ')[0], randPassword),
    };
    await this.emailService.send(confirmationMail);
    delete results.password;
    return results;
  }

  async findAll(options: IPagination): Promise<IPage<User>> {
    const { items, meta } = await paginate(this.userRepository, options, {
      order: {
        name: 'DESC',
      },
      select: ['id', 'email', 'name', 'phone', 'verified', 'active', 'role'],
    });
    return { items, ...meta };
  }

  async findOne(id: number): Promise<User> {
    const user = await this.userRepository.findOne({
      where: {
        id,
        deletedAt: null,
      },
    });
    if (!user) throw new NotFoundException('User with this id not found');
    return user;
  }

  async activate(id: number, { email }: User): Promise<boolean> {
    const user = await this.findOne(id);
    if (user.email === email)
      throw new BadRequestException(
        'This operation is not applicable to yourself',
      );
    user.active = !user.active;
    return (await this.userRepository.save(user)).active;
  }

  async update(
    id: number,
    updateUserAccountDto: UpdateUserAccountDto,
  ): Promise<void> {
    await this.findOne(id);
    await this.userRepository.update(id, updateUserAccountDto);
  }

  async remove(id: number, { email }: User): Promise<void> {
    const user = await this.findOne(id);
    if (!user) {
      throw new NotFoundException('User with this id not found');
    }
    if (user.email === email)
      throw new BadRequestException(
        'This operation is not applicable to yourself',
      );
    await this.userRepository.softDelete(id);
    await this.userRepository.update(id, { email: `${user.email}${id}` });
  }
}
