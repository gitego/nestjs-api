import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Put,
} from '@nestjs/common';
import { ApiExtraModels, ApiTags } from '@nestjs/swagger';
import { Auth } from '../auth/decorators/auth.decorator';
import { GetUser } from '../auth/decorators/get-user.decorator';
import { ERoles } from '../auth/enums';
import {
  CreatedResponse,
  ErrorResponses,
  ForbiddenResponse,
  OkResponse,
  PageResponse,
  Paginated,
  PaginationParams,
  UnauthorizedResponse,
} from '../__shared__/decorators';
import { GenericResponse } from '../__shared__/interfaces/generic-response.interface';
import { IPagination } from '../__shared__/interfaces/page.interface';
import { CreateUserAccountDto } from './dto/create-user-account.dto';
import { UpdateUserAccountDto } from './dto/update-user-account.dto';
import { User } from './entities/user.entity';
import { UserService } from './user.service';

@Controller('users')
@Auth(ERoles.ADMIN)
@ApiTags('Users')
@ApiExtraModels(User)
@ErrorResponses(ForbiddenResponse, UnauthorizedResponse)
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  @CreatedResponse(User)
  async create(
    @Body() createUserAccountDto: CreateUserAccountDto,
  ): Promise<GenericResponse<User>> {
    const results = await this.userService.create(createUserAccountDto);
    return { message: 'User created successfully', results };
  }

  @Get()
  @PageResponse(User)
  @Paginated()
  @HttpCode(HttpStatus.OK)
  async getUsers(@PaginationParams() options: IPagination): Promise<any> {
    const results = await this.userService.findAll(options);
    return { message: 'Users retrieved successfully', results };
  }

  @Patch(':id/activate')
  @OkResponse()
  @HttpCode(HttpStatus.OK)
  async activate(
    @Param('id') id: string,
    @GetUser() user: User,
  ): Promise<GenericResponse<void>> {
    const result = await this.userService.activate(+id, user);
    return {
      message: `User ${result ? 'activated' : 'deactivated'} successfully`,
      results: null,
    };
  }

  @Put(':id')
  @OkResponse()
  @HttpCode(HttpStatus.OK)
  async update(
    @Param('id') id: string,
    @Body() updateUserAccountDto: UpdateUserAccountDto,
  ): Promise<GenericResponse<void>> {
    await this.userService.update(+id, updateUserAccountDto);
    return { message: 'User updated successfully', results: null };
  }

  @Delete(':id')
  @OkResponse()
  @HttpCode(HttpStatus.OK)
  async remove(
    @Param('id') id: string,
    @GetUser() user: User,
  ): Promise<GenericResponse<void>> {
    await this.userService.remove(+id, user);
    return { message: 'User deleted successfully', results: null };
  }
}
