export enum ERoles {
  ADMIN = 'ADMIN',
  CLIENT = 'CLIENT',
}

export enum EOtpType {
  VERIFY_ACCOUNT = 'VERIFY_ACCOUNT',
  RESET_PASSWORD = 'RESET_PASSWORD',
}
