import { HttpModule } from '@nestjs/axios';
import { forwardRef, Global, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import 'dotenv/config';
import { EmailService } from '../communication/email.service';
import { SMSService } from '../communication/sms.service';
import { User } from '../users/entities/user.entity';
import { UserModule } from '../users/user.module';
import { PasswordEncryption } from '../__shared__/utils/PasswordEncryption';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { AuthOtp } from './entities/auth-otp.entity';
import { JwtStrategy } from './jwt.strategy';
import { JwtRefreshStrategy } from './refresh-jwt.strategy';

@Global()
@Module({
  imports: [
    TypeOrmModule.forFeature([User, AuthOtp]),
    forwardRef(() => UserModule),
    PassportModule,
    HttpModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        publicKey: configService.get('jwt').publicKey,
        privateKey: configService.get('jwt').privateKey,
        signOptions: {
          expiresIn: configService.get('jwt').expiresIn,
          issuer: 'tss-api',
          algorithm: 'RS256',
        },
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    JwtStrategy,
    JwtRefreshStrategy,
    PasswordEncryption,
    SMSService,
    ConfigService,
    EmailService,
  ],
  exports: [JwtModule, AuthService],
})
export class AuthModule {}
