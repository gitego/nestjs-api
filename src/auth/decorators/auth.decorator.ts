import { applyDecorators, UseGuards } from '@nestjs/common';
import { ApiCookieAuth } from '@nestjs/swagger';
import { ERoles } from '../enums';
import { JwtGuard } from '../guards/jwt.guard';
import { RolesGuard } from '../guards/roles.guard';
import { Role } from './roles.decorator';

export function Auth(...roles: ERoles[]) {
  return applyDecorators(
    ApiCookieAuth(),
    UseGuards(JwtGuard, RolesGuard),
    Role(...roles),
  );
}
