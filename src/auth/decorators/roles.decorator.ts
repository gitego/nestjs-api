import { SetMetadata } from '@nestjs/common';
import { ERoles } from '../enums';
export const ROLES_KEY = 'roles';

export const Role = (...roles: ERoles[]) => SetMetadata('roles', roles);
