import { OptionalProperty } from '../decorators';
import { ESort } from '../enums';

export class CommonFilterOptionsDto {
  @OptionalProperty()
  dateFrom?: Date;
  @OptionalProperty()
  dateTo?: Date;
  @OptionalProperty()
  search?: string;
  @OptionalProperty({ enum: ESort })
  sort?: ESort;
}
