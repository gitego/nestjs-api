const isValidDate = (d) => {
  return d !== 'Invalid Date' && typeof d !== 'undefined' && d !== '';
};
export default isValidDate;
