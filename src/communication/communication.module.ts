import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { EmailService } from './email.service';
import { SMSService } from './sms.service';

@Module({
  imports: [HttpModule],
  providers: [EmailService, SMSService],
})
export class CommunicationModule {}
